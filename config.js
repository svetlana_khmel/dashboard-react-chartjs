const config = {
  'secret': '',
  'database': '',
  'AUTH0_DOMAIN': 'screwfix.auth0.com',
}

module.exports = config;
