import React, {Component} from 'react';
import {Pie} from 'react-chartjs-2';

class PieChart extends Component {

  render() {

    const { passed, failed } = this.props;

    const data = {
      labels: [
        'Passed',
        'Failed',
      ],
      datasets: [{
        data: [passed, failed],
        backgroundColor: [
          '#36A2EB',
          '#FF6384',

        ],
        hoverBackgroundColor: [
          '#36A2EB',
          '#FF6384',
        ]
      }]
    };

    const options = {
      maintainAspectRatio: true,
      responsive: false,
      legend: {
        fontFamily: "Quicksand, sans-serif",
        fontSize: 30,
        fontColor: '#000',
        padding: 2,
        labels: {
          boxWidth: 10
        }
      }
    }

    return (
      <div className="pie-chart-container">
        <Pie height={250} width={250} options={options} data={data} />
      </div>
    );
  }
}
export default PieChart;
