import React from 'react';

const Item = ({ name, passed, claimed, failed }) => {
  return (

    <li className={'channel-row'}>
      <div className={'name td'}>{ name }</div>
      <div className={'passed td'}>{ passed }</div>
      <div className={'claimed td'}>{ claimed }</div>
      <div className={'failed td'}>{ failed }</div>
    </li>
  )
};

export default Item;