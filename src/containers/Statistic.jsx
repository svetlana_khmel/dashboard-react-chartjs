import React, { PureComponent } from 'react';
import PieChart from './components/PieChart';

export default class Statistic extends PureComponent {
  calculatePassed = () => {
    let {channels} = this.props.data;

      channels ? this.channels = channels.reduce((acc, {passed, failed}) => {

        acc.passed += passed
        acc.failed += failed

        return Object.assign({}, acc);
      }, {
        passed: 0,
        failed: 0
      }) : this.channels = {};

  }

  render () {
    let {channels} = this.props.data;

    this.calculatePassed();

    const { passed, failed } = this.channels;

    return (
      <div className="statistic-block">
          <PieChart passed={ passed } failed={ failed } />
        {channels ? (
          <div className="agenda-block">
            <div className={'td passed'}>Passed: { passed }</div>
            <div className={'td failed'}>Failed: { failed }</div>
          </div>

        ) : (
          <div>Loading...</div>
        )}
      </div>
    )
  }
}

