import React, { Component } from 'react';
import { loadData } from '../actions/actionCreators';
import { connect } from "react-redux";

import { bindActionCreators } from 'redux';
import List from './List';
import Statistic from './Statistic';

class App extends Component {
  componentDidMount () {
    this.props.loadData();
  }

  render () {
    console.log('PROPS 1', this.props.data);
    return (
      <div>
        <h1>CI</h1>
        <Statistic data={this.props.data}></Statistic>
        <List data={this.props.data} />
        <div className={'code-freeze'}>
          Code Freeze: { this.props.data.codeFreeze } {this.props.data.codeFreeze === 1 ? 'day' : 'days'}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log('STATE 1', state);
  return {
    data: state.data
  }
};


export default connect(mapStateToProps, (dispatch)=> bindActionCreators({ loadData }, dispatch))(App);