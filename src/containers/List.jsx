import React, { PureComponent } from 'react';
import Item from './components/item';

export default class List extends PureComponent {
  render () {
    let {channels} = this.props.data;
    console.log("channels _____ ", channels);
    console.log("Props _____ ", this.props.data);

    return (
      <div className="list">
        <div className={'table row'}>
          <div className={'td'}>
            Channel
          </div>
          <div className={'td'}>
            Passed
          </div>
          <div className={'td'}>
            Claimed
          </div>
          <div className={'td'}>
            Failed
          </div>

        </div>
        {channels ? (
          <ul>
            {channels.map((channel, index) => <Item key={index} name={channel.name} passed={channel.passed}
                                                    claimed={channel.claimed} failed={channel.failed}/>)}
          </ul>
        ) : (
          <div>Loading...</div>
        )}
      </div>
    )
  }
}

