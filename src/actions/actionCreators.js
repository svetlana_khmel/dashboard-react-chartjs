import { GET_DATA } from '../actionTypes';
const getDataUrl = `https://private-bf0eb-test12906.apiary-mock.com/ci`;

export function getData (data) {
  console.log('data---', data);
  return {
    type: GET_DATA,
    data
  }
}

export const loadData = () => {
  return (dispatch, getState) => {
    return fetch(getDataUrl, {
      method: 'GET',
      headers: {
        'Accept': 'application.json',
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    })
      .then((data) => {
        dispatch(getData(data));
      })
  }
};