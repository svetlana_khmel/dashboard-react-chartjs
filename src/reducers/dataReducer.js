import { GET_DATA } from '../actionTypes';

let initialState  = {};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_DATA:

      return { ...action.data };

    default:
      return state;
  }
}